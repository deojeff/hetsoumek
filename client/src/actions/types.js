//Auth
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAIL = 'REGISTER_FAIL'
export const USER_LOADED = 'USER_LOADED'
export const USER_LOADING = 'USER_LOADING'
export const AUTH_ERROR = 'AUTH_ERROR'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGOUT = 'LOGOUT'
//Alert
export const SET_ALERT = 'SET_ALERT'
export const REMOVE_ALERT = 'REMOVE_ALERT'
//Loading Screen
export const SET_LOADING = 'SET_LOADING'
export const REMOVE_LOADING = 'REMOVE_LOADING'
//Category
export const GET_CATEGORIES = 'GET_CATEGORIES'
export const CATEGORY_ERROR = 'CATEGORY_ERROR'
export const GET_CATEGORY = 'GET_CATEGORY'
//Product
export const PRODUCT_ERROR = 'PRODUCT_ERROR'
export const GET_PRODUCT = 'GET_PRODUCT'
export const GET_PRODUCTS = 'GET_PRODUCTS'