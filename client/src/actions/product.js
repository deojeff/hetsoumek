import axios from 'axios';
import {
    GET_PRODUCTS,
    GET_PRODUCT,
    PRODUCT_ERROR
} from './types'

export const getProducts = () => async dispatch => {

    try {
        const res = await axios.get('/api/product/')
        dispatch({
            type:GET_PRODUCTS,
            payload: res.data.products
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}

export const getProduct = (id) => async dispatch => {

    try {
        const res = await axios.get(`/api/product/${id}`)
        dispatch({
            type:GET_PRODUCT,
            payload: res.data.product
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}