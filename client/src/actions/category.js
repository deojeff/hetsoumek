import axios from 'axios';
import {
    GET_CATEGORIES,
    GET_CATEGORY,
    CATEGORY_ERROR
} from './types'

export const getCategories = () => async dispatch => {

    try {
        const res = await axios.get('/api/category/')
        dispatch({
            type:GET_CATEGORIES,
            payload: res.data.categories
        })
    } catch (err) {
        dispatch({
            type:CATEGORY_ERROR,
            payload: err
        })
    }
}

export const getCategory = (id) => async dispatch => {

    try {
        const res = await axios.get(`/api/category/${id}`)
        dispatch({
            type:GET_CATEGORY,
            payload: res.data.category
        })
    } catch (err) {
        dispatch({
            type:CATEGORY_ERROR,
            payload: err
        })
    }
}