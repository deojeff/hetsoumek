import axios from 'axios';
import { 
    REGISTER_FAIL,
    REGISTER_SUCCESS,
    USER_LOADED,
    USER_LOADING,
    AUTH_ERROR,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT
 } from './types'

 import setAuthToken from '../utils/setAuthToken'
//Load user

export const loadUser = () => async dispatch => {
    if(localStorage.token){
        setAuthToken(localStorage.token)
    }

    try {
        const res = await axios.get('/api/authcheck')
        dispatch({
            type: USER_LOADED,
            payload: res.data
        })
    } catch (err) {
        dispatch({
            type: AUTH_ERROR
        })
    }
}

 //Register

export const register = ({firstName, lastName, email, phoneNumber, password}) => async dispatch => {
    dispatch({
        type: USER_LOADING
    })
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const body = JSON.stringify({firstName, lastName, email, phoneNumber, password});

    try {
        const res = await axios.post('/api/register', body, config);
        dispatch({
            type: REGISTER_SUCCESS,
        })
    } catch (err) {
        dispatch({
            type: REGISTER_FAIL
        })
    }
}

 //Login

 export const login = (email, password) => async dispatch => {
    dispatch({
        type: USER_LOADING
    })
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const body = JSON.stringify({email, password});

    try {
        const res = await axios.post('/api/login', body, config);
        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        })
        dispatch(loadUser())
    } catch (err) {
        dispatch({
            type: LOGIN_FAIL
        })
    }
}

//Logout

export const logout = () => async dispatch =>{
    dispatch({
        type: USER_LOADING
    })
    dispatch({
        type: LOGOUT
    })
}