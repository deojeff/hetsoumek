import React, { Fragment, useEffect } from 'react'

//Redux
import { Provider } from 'react-redux'
import store from './store'

import Header from './components/components/Header'
import Home from './components/layouts/Home'
import Footer from './components/components/Footer'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Login from './components/layouts/Login'
import Register from './components/layouts/Register'
import setAuthToken from './utils/setAuthToken'
import { loadUser } from './actions/auth'
import Products from './components/layouts/Products'
import Dashboard from './components/layouts/admin/Dashboard'
import Profile from './components/layouts/Profile'
import Contact from './components/layouts/Contact'
import './App.css'
//import 'rsuite/dist/styles/rsuite-default.css'
if (localStorage.token) {
  setAuthToken(localStorage.token)
}
const App = () => {
  useEffect(() => {
    store.dispatch(loadUser())
  }, [])
  return (
    <Provider store={store}>
      <Fragment>
        <Router>
          <Header />
          <Switch>
            <Switch>
              <Route path="/register" component={Register} />
              <Route path="/login" component={Login} />
              <Route path="/product" component={Products} />
              <Route path="/admin" component={Dashboard} />
              <Route path="/profile" component={Profile} />
              <Route path="/contact" component={Contact} />
              <Route path="/" component={Home} />
            </Switch>
          </Switch>
          <Footer />
        </Router>
      </Fragment>
    </Provider>
  )
}

export default App
