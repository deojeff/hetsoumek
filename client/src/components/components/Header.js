import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logout } from "../../actions/auth";
import DropdownRender from "./DropDown";
const Header = ({ logout, auth }) => {
  const clientLinks = (
    <div
      className="menu hidden w-full lg:block  flex-grow lg:flex lg:items-center lg:w-auto lg:px-3 px-8"
      id="nav-content"
    >
      <div className="text-md font-bold text-blue-700 lg:flex-grow">
        <Link
          to="/"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Acceuil
        </Link>
        <Link
          to="/product"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Enchers En Cours
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          Super Offres
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          <DropdownRender />
        </Link>
      </div>

      <div className="relative mx-auto text-gray-600 lg:block hidden">
        <input
          className="border-2 border-gray-300 bg-white h-10 pl-2 pr-8 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          placeholder="Search"
        />
        <button
          type="submit"
          className="absolute right-0 top-0 mt-3 mr-2"
        ></button>
      </div>
      <div className="flex">
        <Link
          to="/"
          onClick={logout}
          className=" block text-md px-4  ml-2 py-2 rounded text-blue-700 font-bold hover:text-white mt-4 hover:bg-blue-700 lg:mt-0"
        >
          Se Déconnecter
        </Link>
      </div>
    </div>
  );
  const sellerLinks = (
    <div
      className="menu hidden w-full lg:block  flex-grow lg:flex lg:items-center lg:w-auto lg:px-3 px-8"
      id="nav-content"
    >
      <div className="text-md font-bold text-blue-700 lg:flex-grow">
        <Link
          to="/"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Acceuil
        </Link>
        <Link
          to="/product"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Enchers En Cours
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          Super Offres
        </Link>
        <Link
          to="/dashboard"
          className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Dashboard
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          <DropdownRender />
        </Link>
      </div>

      <div className="relative mx-auto text-gray-600 lg:block hidden">
        <input
          className="border-2 border-gray-300 bg-white h-10 pl-2 pr-8 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          placeholder="Search"
        />
        <button
          type="submit"
          className="absolute right-0 top-0 mt-3 mr-2"
        ></button>
      </div>
      <div className="flex">
        <Link
          to="/"
          onClick={logout}
          className=" block text-md px-4  ml-2 py-2 rounded text-blue-700 font-bold hover:text-white mt-4 hover:bg-blue-700 lg:mt-0"
        >
          Se Déconnecter
        </Link>
      </div>
    </div>
  );
  const visitorLinks = (
    <div
      className="menu hidden w-full lg:block  flex-grow lg:flex lg:items-center lg:w-auto lg:px-3 px-8"
      id="nav-content"
    >
      <div className="text-md font-bold text-blue-700 lg:flex-grow">
        <Link
          to="/"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Acceuil
        </Link>
        <Link
          to="/product"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Enchers En Cours
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          Super Offres
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          <DropdownRender />
        </Link>
      </div>

      <div className="relative mx-auto text-gray-600 lg:block hidden mr-2">
        <input
          className="border-2 border-gray-300 bg-white h-10 pl-2 pr-8 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          placeholder="Search"
        />
        <button
          type="submit"
          className="absolute right-0 top-0 mt-3 mr-2"
        ></button>
      </div>
      <div className="flex">
        <Link
          to="/login"
          className="block text-md px-4 py-2 rounded text-blue-700 font-bold hover:text-white mt-4 hover:bg-blue-700 lg:mt-0"
        >
          Se Connecter
        </Link>
      </div>
    </div>
  );
  const navSelection = () => {
    if (auth.isAuthenticated) {
      if (auth.user.isSeller) {
        return sellerLinks;
      } else {
        return clientLinks;
      }
    } else {
      return visitorLinks;
    }
  };
  const toggle = () => {
    document.getElementById("nav-content").classList.toggle("hidden");
  };

  return (
    <div>
      <header className="text-gray-700 body-font bg-blue-500  w-full">
        <nav className="flex items-center justify-between flex-wrap bg-white py-4 lg:px-12 shadow border-solid border-t-2 border-blue-700 ">
          <div className="flex justify-between lg:w-auto w-full lg:border-b-0 pl-6 pr-2 border-solid border-b-2 border-gray-300 pb-5 lg:pb-0">
            <div className="flex items-center flex-shrink-0 text-gray-800 mr-16">
              <span className="font-semibold text-xl tracking-tight">
                Het Soumek
              </span>
            </div>
            <div className="block lg:hidden ">
              <button
                id="nav"
                onClick={() => {
                  document
                    .getElementById("nav-content")
                    .classList.toggle("hidden");
                }}
                className="flex items-center px-3 py-2 border-2 rounded text-blue-700 border-blue-700 hover:text-blue-700 hover:border-blue-700"
              >
                <svg
                  className="fill-current h-3 w-3"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>Menu</title>
                  <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                </svg>
              </button>
            </div>
          </div>
          {navSelection()}
        </nav>
        {/* <nav className="flex items-center justify-between flex-wrap bg-blue-500 shadow-lg p-6 border-blue-500 fixed w-full z-10 top-0">
                    <div className="flex items-center flex-shrink-0 text-white mr-6">

                        <a className="text-white no-underline hover:text-white hover:no-underline" href="#">
                            <span className="text-2xl pl-2"><i className="em em-grinning"></i> Het Soumek</span>
                        </a>
                    </div>

                    <div className="block lg:hidden">
                        <button id="nav-toggle" className="flex items-center px-3 py-2 border rounded text-gray-500 border-gray-600 hover:text-white hover:border-white" onClick={() => {
                            document.getElementById("nav-content").classList.toggle("hidden");
                        }}>
                            <svg className="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" /></svg>
                        </button>
                    </div>

                    <div className="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block pt-6 lg:pt-0" id="nav-content">
                        <ul className="list-reset lg:flex justify-end flex-1 items-center">
                            <li className="mr-3">
                                <a className="inline-block text-white no-underline hover:text-blue-800 hover:text-underline py-2 px-4" href="#">Acceuil</a>
                            </li>
                            <li className="mr-3">
                                <a className="inline-block text-white no-underline hover:text-blue-800 hover:text-underline py-2 px-4" href="#">A propos</a>
                            </li>
                            <li className="mr-3">
                                <a className="inline-block text-white no-underline hover:text-blue-800 hover:text-underline py-2 px-4" href="#">Se Connecter</a>
                            </li>

                        </ul>
                    </div>
                </nav> */}
      </header>
    </div>
  );
};
Header.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { logout })(Header);
