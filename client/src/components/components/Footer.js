import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'

const Footer = () => {
    return (
        <Fragment>
            <footer className="text-gray-700 body-font shadow border-solid border-t-2 border-blue-700">
                <div className="container px-5 py-24 mx-auto">
                    <div className="flex flex-wrap md:text-left text-center order-first">
                        <div className="lg:w-1/4 md:w-1/2 w-full px-4">
                            <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">CATEGORIES</h2>
                            <nav className="list-none mb-10">
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>First Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Second Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Third Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Fourth Link</Link>
                                </li>
                            </nav>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 w-full px-4">
                            <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">CATEGORIES</h2>
                            <nav className="list-none mb-10">
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>First Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Second Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Third Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Fourth Link</Link>
                                </li>
                            </nav>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 w-full px-4">
                            <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">CATEGORIES</h2>
                            <nav className="list-none mb-10">
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>First Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Second Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Third Link</Link>
                                </li>
                                <li>
                                    <Link className="text-blue-500 hover:text-gray-600" to='#'>Fourth Link</Link>
                                </li>
                            </nav>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 w-full px-4">
                            <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">SUBSCRIBE</h2>
                            <div className="flex xl:flex-no-wrap md:flex-no-wrap lg:flex-wrap flex-wrap justify-center md:justify-start">
                                <input className="w-40 sm:w-auto bg-gray-100 rounded xl:mr-4 lg:mr-0 sm:mr-4 mr-2 border border-gray-400 focus:outline-none focus:border-blue-500 text-base py-2 px-4" placeholder="Placeholder" type="text" />
                                <button className="lg:mt-2 xl:mt-0 flex-shrink-0 inline-flex text-white bg-blue-700 border-0 py-2 px-6 focus:outline-none hover:bg-blue-800 rounded">Button</button>
                            </div>
                            <p className="text-gray-500 text-sm mt-2 md:text-left text-center">Bitters chicharrones fanny pack
          <br className="lg:block hidden" />waistcoat green juice
        </p>
                        </div>
                    </div>
                </div>
            </footer>
        </Fragment>
    )
}

export default Footer
