import React, { Fragment } from "react";
import {createPopper} from "@popperjs/core";
import { Link } from 'react-router-dom';
const Dropdown = ({ color }) => {
    // dropdown props
    const [dropdownPopoverShow, setDropdownPopoverShow] = React.useState(false);
    const btnDropdownRef = React.createRef();
    const popoverDropdownRef = React.createRef();
    const openDropdownPopover = () => {
        new createPopper(btnDropdownRef.current, popoverDropdownRef.current, {
            placement: "bottom-start"
        });
        setDropdownPopoverShow(true);
    };
    const closeDropdownPopover = () => {
        setDropdownPopoverShow(false);
    };
    // bg colors
    let bgColor;
    color === "white"
        ? (bgColor = "bg-gray-800")
        : (bgColor = "bg-" + color + "-500");
    return (
        <Fragment>
            <button
            className="font-bold"
                style={{ transition: "all .15s ease" }}
                type="button"
                ref={btnDropdownRef}
                onClick={() => {
                    dropdownPopoverShow
                        ? closeDropdownPopover()
                        : openDropdownPopover();
                }}
            >
                {color === "white" ? "Catégories" : color + " Dropdown"}
            </button>
            <div
                ref={popoverDropdownRef}
                className={
                    (dropdownPopoverShow ? "block " : "hidden ") +
                    (color === "white" ? "bg-white " : bgColor + " ") +
                    "text-base z-50 float-left py-2 list-none text-left rounded shadow-lg mt-1"
                }
                style={{ minWidth: "12rem" }}
            >
                <Link
                    to="/products"
                    className={
                        "text-sm py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent hover:bg-blue-700 hover:text-white" +
                        (color === "white" ? " text-gray-800" : "text-white")
                    }

                >
                    Categorie 1
              </Link>
                <Link
                    href="#pablo"
                    className={
                        "text-sm py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent hover:bg-blue-700 hover:text-white" +
                        (color === "white" ? " text-gray-800" : "text-white")
                    }
                    onClick={e => e.preventDefault()}
                >
                    Categorie 2
              </Link>
                <Link
                    to="#pablo"
                    className={
                        "text-sm py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent hover:bg-blue-700 hover:text-white" +
                        (color === "white" ? " text-gray-800" : "text-white")
                    }
                    onClick={e => e.preventDefault()}
                >
                    Categorie 3
              </Link>
                <div className="h-0 my-2 border border-solid border-t-0 border-gray-900 opacity-25" />
                <Link
                    to="#pablo"
                    className={
                        "text-sm py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent hover:bg-blue-700 hover:text-white" +
                        (color === "white" ? " text-gray-800" : "text-white")
                    }
                    onClick={e => e.preventDefault()}
                >
                    Categorie 4
              </Link>
            </div>



        </Fragment>
    );
};

export default function DropdownRender() {
    return (
        <>
            <Dropdown color="white" />
        </>
    );
}
