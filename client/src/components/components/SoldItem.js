import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'

export const SoldItem = () => {
    return (
        <Fragment>
            <div className="lg:w-1/3 md:w-1/2 p-4 w-full bg-gray-200">
                <Link className="block relative h-48 rounded overflow-hidden">
                    <img alt="ecommerce" className="object-cover object-center w-full h-full block" src="https://picsum.photos/420/260" />
                </Link>
                <div className="mt-4">
                    <h3 className="text-gray-500 text-xs tracking-widest title-font mb-1">CATEGORY</h3>
                    <h2 className="text-gray-900 title-font text-lg font-medium">The Catalyzer</h2>
                    <div className="flex ">
                        <div className="flex-initial text-gray-700 text-left  px-5  mt-2">
                            <h1 className="mt-1 text-blue-600 font-medium "> <span className="font-bold"> Reached:</span>17 DT </h1>
                            <h2 className="mt-1 text-gray-600  "> <span className="font-bold"> Original price:</span>17 DT </h2>

                        </div>
                        {/* <div className="flex-initial text-gray-700 text-center  px-6 ">
                            <button className="bg-blue-500 hover:bg-blue-700 text-center text-white font-bold py-2 px-6  rounded inline-flex items-center mt-4">
                                Submit a bid
                                         </button>
                        </div> */}
                    </div>

                    <div className="w-full my-2">
                        {/* <div className="text-right">
                            <p className=" text-blue-800 font-medium my-2 mx-5">Time Left: 00:00:00</p>
                        </div> */}
                        <div className="bg-gray-300 w-full rounded-lg h-2">
                            <div className="bg-blue-600 rounded-lg h-2" style={{ width: "100%" }}></div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
