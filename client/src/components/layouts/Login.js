import React, { Fragment, useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
import img from '../../assets/img/undraw_authentication_fsn5.svg'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {login} from '../../actions/auth'
import Spinner from '../components/Spinner'
const Login = ({auth, login}) => {

    const [FormData, setFormData] = useState({
        email:"",
        password:""
    });
    const{email, password} = FormData;
    const onChange = e => {
        setFormData({...FormData, [e.target.name]: e.target.value})
    }
    const onSubmit = e => {
        e.preventDefault()
        login(email, password);
        setFormData({email:"", password:""})
    }
    if (auth.isAuthenticated){
        return <Redirect to="/"/>
    }
    return (auth.loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) :
        <Fragment>
            <div className="container mx-auto">
                <div className="flex justify-center px-6 my-12">

                    <div className="w-full xl:w-3/4 lg:w-11/12 flex">

                        <div
                            className="w-full h-auto  hidden lg:block lg:w-1/2 bg-cover rounded-l-lg"
                            style={{ backgroundImage: `url(${img})`, backgroundSize: "90%", backgroundRepeat: 'no-repeat' }}
                        // style="background-image: url('https://source.unsplash.com/K4mSJ7kc0As/600x800')"
                        ></div>

                        <div className="w-full lg:w-1/2 bg-white p-5 rounded-lg lg:rounded-l-none">
                            <h3 className="pt-4 text-2xl text-center">Welcome Back!</h3>
                            <form className="px-8 pt-6 pb-8 mb-4 bg-white rounded" onSubmit={e => onSubmit(e)}>
                                <div className="mb-4">
                                    <label className="block mb-2 text-sm font-bold text-gray-700" htmlFor="email">
                                        Email
								</label>
                                    <input
                                        className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                        id="email"
                                        name="email"
                                        type="email"
                                        placeholder="Email"
                                        value= {email}
                                        onChange={e => onChange(e)}
                                        required
                                    />
                                </div>
                                <div className="mb-4">
                                    <label className="block mb-2 text-sm font-bold text-gray-700" htmlFor="password">
                                        Password
								</label>
                                    <input
                                        className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                        id="password"
                                        name="password"
                                        type="password"
                                        placeholder="password"
                                        value= {password}
                                        onChange={e => onChange(e)}
                                        required
                                    />
                                    {/* <p className="text-xs italic text-red-500">Please choose a password.</p> */}
                                </div>
                                {/* <div className="mb-4">
                                    <input className="mr-2 leading-tight" type="checkbox" id="checkbox_id" />
                                    <label className="text-sm" for="checkbox_id">
                                        Remember Me
								</label>
                                </div> */}
                                <div className="mb-6 text-center">
                                    <button
                                        className="w-full px-4 py-2 font-bold text-white bg-blue-500 rounded-full hover:bg-blue-700 focus:outline-none focus:shadow-outline"
                                        type="submit"
                                    >
                                        Sign In
								</button>
                                </div>
                                <hr className="mb-6 border-t" />
                                <div className="text-center">
                                    <Link
                                        className="inline-block text-sm text-blue-500 align-baseline hover:text-blue-800"
                                        to="/register"
                                    >
                                        Create an Account!
								</Link>
                                </div>
                                <div className="text-center">
                                    <Link
                                        to="#"
                                        className="inline-block text-sm text-blue-500 align-baseline hover:text-blue-800"
                                    >
                                        Forgot Password?
								</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

Login.propTypes = {
    login: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
}

const mapStateToProps = state =>({
    auth: state.auth,
})
export default connect(mapStateToProps, {login})(Login);
