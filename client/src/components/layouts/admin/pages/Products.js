import React, { useState } from 'react'
import { Modal, Button, Input } from 'rsuite'
const Products = () => {
  const [visible, setVisible] = useState(false)
  const dialog = document.getElementById('modal')

  return (
    <div class="mb-2 -mx-3">
      <div class="w-full p-3">
        <div class="bg-white rounded-md shadow">
          <div class="px-5 py-4 font-bold bg-gray-100 border-b text-theme-color text-md rounded-t-md flex">
            <button
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded content-end float-right"
              onClick={(e) => {
                setVisible(true)
                e.preventDefault()
                console.log(visible)
              }}
            >
              Ajouter
            </button>
          </div>

          <div id="modal" className={` ${visible ? '' : 'hidden'}`}>
            <div class="flex items-center justify-center fixed left-0 bottom-0 w-full h-full ">
              <div class="bg-white rounded-lg w-1/2">
                <div class="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md">
                  <h2 class="text-lg text-gray-700 font-semibold capitalize">
                    Account settings
                  </h2>

                  <form>
                    <div class="grid grid-cols-1 sm:grid-cols-2 gap-6 mt-4">
                      <div>
                        <label class="text-gray-700" for="name">
                          Product Name
                        </label>
                        <input
                          id="name"
                          type="text"
                          class="w-full mt-2 px-4 py-2 block rounded bg-gray-200 text-gray-800 border border-gray-300 focus:outline-none focus:bg-white"
                        />
                      </div>

                      <div>
                        <label class="text-gray-700" for="category">
                          Category
                        </label>
                        <input
                          id="category"
                          type="text"
                          class="w-full mt-2 px-4 py-2 block rounded bg-gray-200 text-gray-800 border border-gray-300 focus:outline-none focus:bg-white"
                        />
                      </div>

                      <div>
                        <label class="text-gray-700" for="originalprice">
                          Original Price
                        </label>
                        <input
                          id="originalprice"
                          type="text"
                          class="w-full mt-2 px-4 py-2 block rounded bg-gray-200 text-gray-800 border border-gray-300 focus:outline-none focus:bg-white"
                        />
                      </div>

                      <div>
                        <label class="text-gray-700" for="bidprice">
                          Bid Starting Price
                        </label>
                        <input
                          id="bidprice"
                          type="text"
                          class="w-full mt-2 px-4 py-2 block rounded bg-gray-200 text-gray-800 border border-gray-300 focus:outline-none focus:bg-white"
                        />
                      </div>
                    </div>

                    <div class="flex justify-end mt-4">
                      <button
                        class="px-4 py-2 bg-blue-500 text-white rounded hover:bg-gray-700 focus:outline-none focus:bg-blue-600"
                        onClick={() => {
                          setVisible(false)
                        }}
                      >
                        Save
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div class="pt-3 text-gray-700 text-md">
            <table class="w-full text-left">
              <thead>
                <tr class="bg-gray-50">
                  <th class="px-4 py-2">Name</th>
                  <th class="px-4 py-2">Position</th>
                  <th class="px-4 py-2">Office</th>
                  <th class="px-4 py-2">Age</th>
                  <th class="px-4 py-2">Start Date</th>
                  <th class="px-4 py-2">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">
                    {' '}
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-eye"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="py-2 m-2 text-right">
              <span class="relative inline-flex text-gray-600 -z-1">
                <button
                  type="button"
                  class="relative inline-flex items-center px-2 py-2 text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-l-md hover:text-gray-500 focus:outline-none "
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-4 h-4"
                  >
                    <path d="M15 19l-7-7 7-7"></path>
                  </svg>
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
                >
                  1
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
                >
                  2
                </button>
                <button
                  type="button"
                  class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
                >
                  4
                </button>
                <button
                  type="button"
                  class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
                >
                  5
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-2 py-2 -ml-px text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-r-md hover:text-gray-500 focus:outline-none "
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-4 h-4"
                  >
                    <path d="M9 5l7 7-7 7"></path>
                  </svg>
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Products
