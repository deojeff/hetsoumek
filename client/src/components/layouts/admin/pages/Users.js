import React from 'react'

const Users = () => {
  return (
    <div class="mb-2 -mx-3">
      <div class="w-full p-3">
        <div class="bg-white rounded-md shadow">
          <div class="px-5 py-4 font-bold bg-gray-100 border-b text-theme-color text-md rounded-t-md flex">
            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded content-end">
              Ajouter
            </button>
          </div>
          <div class="pt-3 text-gray-700 text-md">
            <table class="w-full text-left">
              <thead>
                <tr class="bg-gray-50">
                  <th class="px-4 py-2">Name</th>
                  <th class="px-4 py-2">Position</th>
                  <th class="px-4 py-2">Office</th>
                  <th class="px-4 py-2">Age</th>
                  <th class="px-4 py-2">Start Date</th>
                  <th class="px-4 py-2">Salary</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="px-4 py-2">Jennifer Chang</td>
                  <td class="px-4 py-2">Regional Director</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">28</td>
                  <td class="px-4 py-2">2010/11/14</td>
                  <td class="px-4 py-2">$357,650</td>
                </tr>
                <tr class="bg-gray-50">
                  <td class="px-4 py-2">Jonas Alexander</td>
                  <td class="px-4 py-2">Developer</td>
                  <td class="px-4 py-2">San Francisco</td>
                  <td class="px-4 py-2">30</td>
                  <td class="px-4 py-2">2010/07/14</td>
                  <td class="px-4 py-2">$86,500</td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Lael Greer</td>
                  <td class="px-4 py-2">Systems Administrator</td>
                  <td class="px-4 py-2">London</td>
                  <td class="px-4 py-2">21</td>
                  <td class="px-4 py-2">2009/02/27</td>
                  <td class="px-4 py-2">$103,500</td>
                </tr>
                <tr class="bg-gray-50">
                  <td class="px-4 py-2">Martena Mccray</td>
                  <td class="px-4 py-2">Post-Sales support</td>
                  <td class="px-4 py-2">Edinburgh</td>
                  <td class="px-4 py-2">46</td>
                  <td class="px-4 py-2">2011/03/09</td>
                  <td class="px-4 py-2">$324,050</td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Michael Bruce</td>
                  <td class="px-4 py-2">Javascript Developer</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">29</td>
                  <td class="px-4 py-2">2011/06/27</td>
                  <td class="px-4 py-2">$183,000</td>
                </tr>
                <tr class="bg-gray-50">
                  <td class="px-4 py-2">Michael Silva</td>
                  <td class="px-4 py-2">Marketing Designer</td>
                  <td class="px-4 py-2">London</td>
                  <td class="px-4 py-2">66</td>
                  <td class="px-4 py-2">2012/11/27</td>
                  <td class="px-4 py-2">$198,500</td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Michelle House</td>
                  <td class="px-4 py-2">Integration Specialist</td>
                  <td class="px-4 py-2">Sydney</td>
                  <td class="px-4 py-2">37</td>
                  <td class="px-4 py-2">2011/06/02</td>
                  <td class="px-4 py-2">$95,400</td>
                </tr>
                <tr class="bg-gray-50">
                  <td class="px-4 py-2">Olivia Liang</td>
                  <td class="px-4 py-2">Support Engineer</td>
                  <td class="px-4 py-2">Singapore</td>
                  <td class="px-4 py-2">64</td>
                  <td class="px-4 py-2">2011/02/03</td>
                  <td class="px-4 py-2">$234,500</td>
                </tr>
                <tr>
                  <td class="px-4 py-2">Paul Byrd</td>
                  <td class="px-4 py-2">Chief Financial Officer (CFO)</td>
                  <td class="px-4 py-2">New York</td>
                  <td class="px-4 py-2">64</td>
                  <td class="px-4 py-2">2010/06/09</td>
                  <td class="px-4 py-2">$725,000</td>
                </tr>
                <tr class="bg-gray-50">
                  <td class="px-4 py-2">Prescott Bartlett</td>
                  <td class="px-4 py-2">Technical Author</td>
                  <td class="px-4 py-2">London</td>
                  <td class="px-4 py-2">27</td>
                  <td class="px-4 py-2">2011/05/07</td>
                  <td class="px-4 py-2">$145,000</td>
                </tr>
              </tbody>
            </table>
            <div class="py-2 m-2 text-right">
              <span class="relative inline-flex text-gray-600 -z-1">
                <button
                  type="button"
                  class="relative inline-flex items-center px-2 py-2 text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-l-md hover:text-gray-500 focus:outline-none "
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-4 h-4"
                  >
                    <path d="M15 19l-7-7 7-7"></path>
                  </svg>
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
                >
                  1
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
                >
                  2
                </button>
                <button
                  type="button"
                  class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
                >
                  4
                </button>
                <button
                  type="button"
                  class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
                >
                  5
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-2 py-2 -ml-px text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-r-md hover:text-gray-500 focus:outline-none "
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-4 h-4"
                  >
                    <path d="M9 5l7 7-7 7"></path>
                  </svg>
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Users
