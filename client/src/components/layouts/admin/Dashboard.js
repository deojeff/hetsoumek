import React from 'react'
import Sidebar from './components/Sidebar'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  useRouteMatch,
  Link,
} from 'react-router-dom'
import Settings from './pages/Settings'
import Users from './pages/Users'
import Products from './pages/Products'
import Transactions from './pages/Transactions'
import Bids from './pages/Bids'
import Commandes from './pages/Commandes'
import Feedback from './pages/Feedback'
const Dashboard = () => {
  let match = useRouteMatch()
  return (
    <div class="flex">
      <div class="fixed z-20 inset-0 bg-white opacity-50 transition-opacity shadow border-solid  lg:hidden"></div>
      

      <div class="fixed z-30 inset-y-0 left-0 w-64 transition duration-300 transform bg-white overflow-y-auto lg:translate-x-0 lg:static lg:inset-0 border-r-2 border-blue-700">
        <nav class="mt-10">
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to={`${match.url}`}
          >
            <i class="fas fa-box"></i>

            <span class="mx-4">Produits</span>
          </Link>
          <Link
            to={`${match.url}/settings`}
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          >
            <i class="fas fa-user-cog"></i>

            <span class="mx-4">Mon Compte</span>
          </Link>
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to={`${match.url}/commandes`}
          >
            <i class="fas fa-money-check"></i>

            <span class="mx-4"> Commandes</span>
          </Link>
          <Link
            to={`${match.url}/users`}
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          >
            <i class="fas fa-user-friends"></i>

            <span class="mx-4">Utilisateurs</span>
          </Link>
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to={`${match.url}/transactions`}
          >
            <i class="fas fa-arrows-alt-h"></i>

            <span class="mx-4">Transactions</span>
          </Link>
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to={`${match.url}/bids`}
          >
            <i class="fas fa-gavel"></i>

            <span class="mx-4">Enchers</span>
          </Link>
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to={`${match.url}/feedback`}
          >
            <i class="fas fa-comment-dots"></i>

            <span class="mx-4">Réclamations</span>
          </Link>
        </nav>
      </div>

      <div class="flex-1  bg-gray-100 ">
        <div class="flex-1 px-6 mb-4">
          <h1 class="mb-2 text-3xl text-gray-700">Tables</h1>

          {/**Routing Goes here */}
          <Switch>
            <Route path={`${match.path}/users`} component={Users} />
            <Route
              path={`${match.path}/transactions`}
              component={Transactions}
            />
            <Route path={`${match.path}/settings`} component={Settings} />
            <Route path={`${match.path}/feedback`} component={Feedback} />
            <Route path={`${match.path}/bids`} component={Bids} />
            <Route path={`${match.path}/commandes`} component={Commandes} />
            <Route path={`${match.path}/`} component={Products} />
          </Switch>
        </div>
      </div>
    </div>
  )
}

export default Dashboard
