import React, { Fragment, useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
import img from '../../assets/img/undraw_authentication_fsn5.svg'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {register} from '../../actions/auth'
import Spinner from '../components/Spinner'
const Register = ({auth, register}) => {
    const [FormData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber:"",
        password: "",
        password2:""
    })

    const {firstName, lastName, email, phoneNumber, password, password2} = FormData

    const onChange = e =>{
        setFormData({...FormData, [e.target.name]: e.target.value})
    }

    const onSubmit = e =>{
        e.preventDefault()
        if(password !== password2){
            setFormData({password:"", password2:""})
        }else{
             register({firstName, lastName, email, phoneNumber, password});
             setFormData({firstName: e.target.firstName.value, lastName: e.target.lastName.value, email:"", phoneNumber:"", password:"", password2:""})
             return <Redirect to="/"/>
        }
     }
     if(auth.isAuthenticated){
         return <Redirect to="/"/>
     }
    return (auth.loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) :(
        <Fragment>
            <div className="container mx-auto">
                <div className="flex justify-center px-6 my-12">

                    <div className="w-full xl:w-3/4 lg:w-11/12 flex">

                        <div
                            className="w-full h-auto  hidden lg:block lg:w-1/2 bg-cover rounded-l-lg"
                            style={{ backgroundImage: `url(${img})`, backgroundSize: "90%", backgroundRepeat: 'no-repeat' }}
                        // style="background-image: url('https://source.unsplash.com/K4mSJ7kc0As/600x800')"
                        ></div>

                        <div className="w-full lg:w-1/2 bg-white p-5 rounded-lg lg:rounded-l-none">
                            <h3 className="pt-4 text-2xl text-center">Welcome Back!</h3>
                            <form className="px-8 pt-6 pb-8 mb-4 bg-white rounded" onSubmit={e => onSubmit(e)}>
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="firstName">
                                    First Name
                                </label>
                                <input className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                    id="firstName"
                                    name="firstName"
                                    type="text"
                                    placeholder="Foulen"
                                    value={firstName}
                                    onChange={e => onChange(e)}
                                    required
                                    />
                                </div>
                                <div className="w-full md:w-1/2 px-3">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="lastName">
                                    Last Name
                                </label>
                                <input className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                    id="lastName"
                                    name="lastName"
                                    type="text"
                                    placeholder="Faltani"
                                    value={lastName}
                                    onChange={e => onChange(e)}
                                    required
                                    />
                                </div>
                            </div>
                                <div className="mb-4">
                                    <label className="block mb-2 text-sm font-bold text-gray-700" htmlFor="email">
                                        Email
								</label>
                                    <input className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                    id="email"
                                    type="email"
                                    name="email"
                                    placeholder="example@example.com"
                                    value={email}
                                    onChange={e => onChange(e)}
                                    required
                                    />
                                </div>
                                <div className="mb-4">
                                    <label className="block mb-2 text-sm font-bold text-gray-700" htmlFor="phoneNumber">
                                        Phone
								</label>
                                    <input className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                        id="phoneNumber"
                                        type="text"
                                        name="phoneNumber"
                                        placeholder="Phone Number"
                                        value={phoneNumber}
                                        onChange={e => onChange(e)}
                                        required
                                    />
                                </div>
                                <div className="mb-4">
                                    <label className="block mb-2 text-sm font-bold text-gray-700" htmlFor="password">
                                        Password
								</label>
                                    <input className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                        id="password"
                                        type="password"
                                        name="password"
                                        placeholder="password"
                                        value={password}
                                        onChange={e => onChange(e)}
                                        required
                                    />
                                    {/* <p className="text-xs italic text-red-500">Please choose a password.</p> */}
                                </div>
                                <div className="mb-4">
                                    <label className="block mb-2 text-sm font-bold text-gray-700" htmlFor="password2">
                                        Confirm Password
								</label>
                                    <input className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                        id="password2"
                                        type="password"
                                        name="password2"
                                        placeholder="Confirm Password"
                                        value={password2}
                                        onChange={e => onChange(e)}
                                        required
                                    />
                                    {/* <p className="text-xs italic text-red-500">Please choose a password.</p> */}
                                </div>
                                {/* <div className="mb-4">
                                    <input className="mr-2 leading-tight" type="checkbox" id="checkbox_id" />
                                    <label className="text-sm" htmlFor="checkbox_id">
                                        Remember Me
								</label>
                                </div> */}
                                <div className="mb-6 text-center">
                                    <button
                                        className="w-full px-4 py-2 font-bold text-white bg-blue-500 rounded-full hover:bg-blue-700 focus:outline-none focus:shadow-outline"
                                        type="submit"
                                    >
                                        Sign Up
								</button>
                                </div>
                                <hr className="mb-6 border-t" />
                                <div className="text-center">
                                    <Link
                                        className="inline-block text-sm text-blue-500 align-baseline hover:text-blue-800"
                                        to="/login"
                                    >
                                        Sign In!
								</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>)
    )
}
Register.propTypes = {
    register: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
}

const mapStateToProps = state =>({
    auth: state.auth,
})
export default connect(mapStateToProps, {register})(Register);
