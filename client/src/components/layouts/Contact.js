import React from "react";

const Contact = () => {
  return (
    <div>
      <section class="text-gray-700 body-font relative">
        <div class="container px-5 py-24 mx-auto">
          <div class="flex flex-col text-center w-full mb-12">
            <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
              Contact Us
            </h1>
            <p class="lg:w-2/3 mx-auto leading-relaxed text-base">
              Whatever cardigan tote bag tumblr hexagon brooklyn asymmetrical
              gentrify.
            </p>
          </div>
          <div class="lg:w-1/2 md:w-2/3 mx-auto">
            <div class="flex flex-wrap -m-2">
              <div class="p-2 w-2/3">
                <div className="py-2">
                  <input
                    class="w-full  bg-gray-100 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2"
                    placeholder="Telephone"
                    type="text"
                  />
                </div>{" "}
                <div className="py-2">
                  <input
                    class="w-full  bg-gray-100 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2"
                    placeholder="Email"
                    type="email"
                  />
                </div>{" "}
                <div className="py-2">
                  <input
                    class="w-full  bg-gray-100 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2"
                    placeholder="Adresse"
                    type="text"
                  />
                </div>
                <div className="py-2">
                  <div class="inline-block relative w-64">
                    <select class="w-full  bg-gray-100 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2">
                      <option>
                        Really long option that will likely overlap the chevron
                      </option>
                      <option>Option 2</option>
                      <option>Option 3</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="p-2 w-full">
                <textarea
                  class="w-full bg-gray-100 rounded border border-gray-400 focus:outline-none h-48 focus:border-indigo-500 text-base px-4 py-2 resize-none block"
                  placeholder="Message"
                ></textarea>
              </div>
              <div class="py-2 w-full">
                <div class="">
                  <div class="flex -mx-2">
                    <div class="w-1/3 px-2">
                      <button class="flex mx-auto text-white bg-red-500 border-0 py-2 px-8 focus:outline-none hover:bg-red-600 rounded text-lg">
                        Annuler
                      </button>
                    </div>
                    <div class="w-1/3 px-2">
                      <button class="flex mx-auto text-white bg-blue-500 border-0 py-2 px-8 focus:outline-none hover:bg-blue-600 rounded text-lg">
                        Envoyer
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Contact;
