import React, { Fragment, useEffect } from 'react'
import img from '../../assets/img/illustration.png'
import Item from '../components/Item';
import { SoldItem } from '../components/SoldItem';
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getProducts} from '../../actions/product'
const Home = ({auth, product, getProducts}) => {
    useEffect(() => {
        getProducts()
    }, [getProducts]);
    return (
        <Fragment>
            <section class="text-gray-700 body-font">
            {
                !auth.isAuthenticated && (
                    <Fragment>
                    <div class="container flex flex-wrap px-5 py-24 mx-auto items-center">
                    <div class="md:w-1/2 md:pr-12 md:py-8 md:border-r md:border-b-0 mb-10 md:mb-0 pb-10 ">
                        <h1 class="sm:text-5xl text-2xl font-medium title-font mb-2 text-gray-900">Everything has a value</h1>
                        <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">The best place to bid</h1>
                        <button class="bg-blue-500 hover:bg-blue-700 text-center text-white font-bold py-2 px-4  rounded inline-flex items-center mt-4">
                            Add goods to sell
                          </button>
                    </div>
                    <div class="flex flex-col md:w-1/2 md:pl-12">
                        <img src={img} style={{ height: "80%", width: "80%" }} />
                    </div>
                </div>
                </Fragment>
                )
            }

            </section>
            <section class="text-gray-700 body-font">

                <div class="container px-5 py-2 mx-auto">
                    <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">Ending today offers</h1>
                    <div class="flex flex-wrap mt-5">
                    {product.products && product.products.map(product =>{
                        return(
                            <Item key={product._id} product={product} />
                        )
                    })}
                        
                    </div>
                </div>
            </section>
            <section class="text-gray-700 body-font">

                <div class="container px-5 py-10 mx-auto">
                    <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">New offers</h1>
                    <div class="flex flex-wrap mt-5">
                        <Item />
                        <Item />
                        <Item />
                        <Item />
                        <Item />
                        <Item />
                    </div>
                </div>
            </section>
            <section class="text-gray-700 body-font">

                <div class="container px-5 py-10 mx-auto">
                    <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">Recently Sold</h1>
                    <div class="flex flex-wrap mt-5">
                       <SoldItem/>
                       <SoldItem/>
                       <SoldItem/>
                       <SoldItem/>
                       <SoldItem/>
                       <SoldItem/>
                       <SoldItem/>
                       <SoldItem/>
                       <SoldItem/>

                    </div>
                </div>
            </section>

        </Fragment>
    )
}
Home.propTypes = {
    auth: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
    getProducts: PropTypes.func.isRequired,
  }
  const mapStateToProps = state =>({
    auth: state.auth,
    product:state.product
  })
export default connect(mapStateToProps, {getProducts})(Home)
