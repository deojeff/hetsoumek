import React from 'react'
import Item from '../components/Item';
const Products = () => {
    return (
        <section class="text-gray-700 body-font">

            <div class="container px-5 py-10 mx-auto items-center">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">New offers</h1>
                <div class="flex flex-wrap mt-5">
                    <Item />
                    <Item />
                    <Item />
                    <Item />
                    <Item />
                    <Item />
                </div>
            </div>
        </section>
    )
}

export default Products
