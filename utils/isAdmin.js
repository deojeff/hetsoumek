module.exports = function ( req, res, next ){
	const user = req.verifiedUser;
	if( user.isAdmin )
		next();
	else{
		res.status( 403 ).json( { message: "Forbidden access, only Sellers" } );
	}
};