const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const BidSchema = new Schema( {
	bidder: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
	priceBefore:{ type: Number, default:0, required: true },
	priceAfter:{ type: Number, default:0, required: true },
	bidMuch: { type: Number, default:0, required: true },
},{ timestamps: true } );

module.exports = mongoose.model( "Bid", BidSchema ) || mongoose.models.Bid;