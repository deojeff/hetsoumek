const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const Schema = mongoose.Schema;

const CategorySchema = new Schema( {
	categoryName:{ type: String, min: 4, max: 255, lowercase:true, index:true, required:true, unique:true },
	about: { type: String, maxlength: 512 }
},{ timestamps: true }
);

CategorySchema.plugin( uniqueValidator, { message: "is already taken." } );

module.exports = mongoose.model( "Category", CategorySchema ) || mongoose.models.Category;
