const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const Schema = mongoose.Schema;

const UserSchema = new Schema( {
	avatar: { type: String },
	firstName: { type: String, max: 64 },
	lastName: { type:String, max: 64 },
	email: { type: String, required:[ true, "can't be blank" ], index:true, lowercase: true, unique: true, },
	phoneNumber: { type: Number },
	address: { type: String },
	password: { type: String, required: [ true, "can't be blank" ], max: 1024 },
	isAdmin: { type: Boolean, default: false },
	isSeller: { type: Boolean, default: false }
}, { timestamps: true } );

UserSchema.plugin( uniqueValidator, { message: "is already taken." } );

UserSchema.methods.authToJSON = function(){
	return {
		_id: this._id,
		avatar: this.avatar,
		email: this.email,
		firstName: this.firstName,
		lastName: this.lastName,
		isAdmin: this.isAdmin,
		isSeller: this.isSeller
	};
};
UserSchema.methods.profileToJSON = function(){

	return {
		_id: this._id,
		firstName: this.firstName,
		lastName: this.lastName,
		avatar: this.avatar
	};
};

UserSchema.methods.userProfile = function(){
	return{
		_id: this._id,
		firstName: this.firstName,
		lastName: this.lastName,
		email: this.email,
		avatar: this.avatar,
		address: this.address,
		phoneNumber: this.phoneNumber,
		isAdmin: this.isAdmin,
		isSeller: this.isSeller
	};
};
module.exports = mongoose.model( "User", UserSchema ) || mongoose.models.User;