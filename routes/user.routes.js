const router = require( "express" ).Router();
const userControllers = require( "../controllers/user.controllers" );
const User = require( "../models/user.models" );
const verifyToken = require( "../utils/verifyToken" );
const isAdmin = require( "../utils/isAdmin" );
router.param( "user", async( req, res,next, id )=>{
	try{
		const user = await User.findById( id );
		if( !user ) return res.sendStatus( 404 );
		req.user = user;
		return next();

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.get( "/currentuser", verifyToken, userControllers.currentUser );
router.get( "/:user", verifyToken, isAdmin, userControllers.getUser );
router.get( "/", verifyToken, isAdmin, userControllers.getUsers );
router.put( "/:user/update", verifyToken, userControllers.updateUser );
router.delete( "/:user/delete", verifyToken, isAdmin, userControllers.deleteUser );

module.exports = router;