const router = require( "express" ).Router();
const productControllers = require( "../controllers/product.controllers" );
const Product = require( "../models/product.models" );
const verifyToken = require( "../utils/verifyToken" );
const isSeller = require( "../utils/isSeller" );

const multer = require( "multer" );

const storage = multer.diskStorage( {
	destination: function( req, file, cb ) {
		cb( null, "./uploads/" );
	},
	filename: function( req, file, cb ) {
		cb( null, new Date().toISOString() + file.originalname );
	}
} );

const fileFilter = ( req, file, cb ) => {
	// reject a file
	if ( file.mimetype === "image/jpeg" || file.mimetype === "image/png" ) {
		cb( null, true );
	} else {
		cb( null, false );
	}
};

const upload = multer( {
	storage: storage,
	fileFilter: fileFilter
} );

router.param( "product", async( req, res,next, id )=>{
	try{
		const product = await Product.findById( id );
		if( !product ) return res.sendStatus( 404 );
		req.product = product;
		return next();

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.get( "/:product", productControllers.getProduct );
router.get( "/", productControllers.getProducts );
router.post( "/create", upload.single( "productImage" ), verifyToken, isSeller, productControllers.createProduct );
router.put( "/:product/update", upload.single( "productImage" ), verifyToken, isSeller, productControllers.updateProduct );
router.delete( "/:product/delete", verifyToken, isSeller, productControllers.deleteProduct );

module.exports = router;