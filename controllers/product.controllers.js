const Product = require( "../models/product.models" );

const createProduct = async ( req, res )=>{

	const product = new Product( {
		productName: req.body.productName,
		description: req.body.description,
		originalPrice: req.body.originalPrice,
		basePrice: req.body.basePrice,
		productImage: req.file.filename,
		quantity: req.body.quantity,
		category: req.body.category,
		seller: req.body.seller,
		startBidTime: req.body.startBidTime,
		endBidTime: req.body.endBidTime
	} );
	
	try {
		const savedProduct = await product.save();
		return res.status( 200 ).json( { savedProduct: savedProduct } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

//Get all products
const getProducts = async ( req, res ) => {
	try{
		const products = await Product.find().populate( { path: "seller", select:"_id" } );
		return res.status( 200 ).json( { products: products } );
	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
};
//Get single product
const getProduct = async( req, res )=> {

	const product = req.product;

	try{
		return res.status( 200 ).json( { product: product } );

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
};
//update product
const updateProduct = async( req, res )=>{
	const product = req.product;
	try {
		const dataToUpdate = req.body;
		let { ...updateData } = dataToUpdate;
		if( req.file !== undefined ){
			updateData = { ...updateData, productImage:req.file.filename };
		}
		const updateProduct = await Product.findByIdAndUpdate( product._id, updateData, { new: true } );
		return res.status( 200 ).json( { updateProduct: updateProduct } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
//Get delete product
const deleteProduct = async( req, res )=>{
	try {
		const product = req.product;
		const deleteProduct = await Product.findByIdAndDelete( product._id );
		return res.status( 200 ).json( { deleteProduct: deleteProduct } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

module.exports.createProduct = createProduct;
module.exports.getProduct = getProduct;
module.exports.getProducts = getProducts;
module.exports.updateProduct = updateProduct;
module.exports.deleteProduct = deleteProduct;