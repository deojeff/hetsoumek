const Category = require( "../models/category.models" );

const createCategory = async( req, res )=>{

	const category = new Category( {
		categoryName: req.body.category,
		about: req.body.about
	} );

	try {
		const savedCategory = await category.save();
		return res.status( 200 ).json( { savedCategory: savedCategory } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const getCategory = async( req, res )=>{

	const category = req.category;

	try {
		return res.status( 200 ).json( { category: category } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const getCategories = async( req, res )=>{

	try {
		const categories = await Category.find();
		return res.status( 200 ).json( { categories: categories } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const updateCategory = async( req , res )=> {

	const category= req.category;

	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updateCategory = await Category.findByIdAndUpdate( category._id, updateData, { new :true } );
		return res.status( 200 ).json( { updateCategory: updateCategory } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const deleteCategory = async( req, res )=>{
	const category = req.category;

	try {
		const deleteCategory = await Category.findByIdAndDelete( category._id );
		return res.status( 200 ).json( { deleteCategory: deleteCategory } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

module.exports.createCategory = createCategory;
module.exports.getCategory = getCategory;
module.exports.getCategories = getCategories;
module.exports.updateCategory = updateCategory;
module.exports.deleteCategory = deleteCategory;