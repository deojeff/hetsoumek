const User = require( "../models/user.models" );


const currentUser = async( req, res ) => {

	try {
		const user = await User.findById( req.verifiedUser._id );
		return res.status( 200 ).json( user.userProfile() );
	} catch ( err ) {
		return res.status( 401 ).json( { err_message: err } );
	}
};
const getUsers = async( req, res )=>{

	try {
		const users = await User.find();
		users.forEach( ( user )=>{return res.status( 200 ).json( user.authToJSON() );} );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const getUser = async( req, res )=>{

	const id = req.body.id;

	try {
		const user = await User.findById( id );
		return res.status( 200 ).json( user.authToJSON() );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const updateUser = async( req, res )=>{
	const id = req.body.id;
	try {
		const dataToUpdate = req.body;
		let { ...updateData } = dataToUpdate;
		if( req.file !== undefined ){
			updateData = { ...updateData, avatar:req.file.filename };
		}
		const updateUser = await User.findOneAndUpdate( id, updateData, { new: true } );
		return res.status( 200 ).json( { updateUser: updateUser } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const deleteUser = async( req, res )=>{
	const id = req.body.id;

	try {
		const deleteUser = await User.findByIdAndDelete( id );
		return res.status( 200 ).json( { deleteUser: deleteUser } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.currentUser = currentUser;
module.exports.getUser = getUser;
module.exports.getUsers = getUsers;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;